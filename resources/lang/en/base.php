<?php

return [
	'alerts'    => [
		'default' => [
			'title' => 'Information',
	    	'class'	=> 'info',
	    	'icon'	=> 'fa fa-info',
	    	'message' => 'Null'
		],

	    'success' => [
	    	'title' => 'Request Success',
	    	'icon'	=> 'fa fa-check',
	    	'class'	=> 'success',
	    	'messages' => [
	    		'created'             => ':attribute has been successfully created.'  ,
			    'deleted'             => ':attribute has been successfully deleted.'  ,
			    'deleted_permanently' => ':attribute has been deleted permanently.'   ,
			    'restored'            => ':attribute has been successfully restored.' ,
			    'updated'             => ':attribute has been successfully updated.'  ,
	    	]
	    ],
	    'failed' => [
	    	'title' => 'Request Failed',
	    	'icon'	=> 'fa fa-times',
	    	'class' => 'danger',
	    	'messages' => [
	    		'created'             => 'There was an error in creating the :attribute.'  ,
			    'deleted'             => 'There was an error in deleting the :attribute.'  ,
			    'deleted_permanently' => 'There was an error in deleting permanently the :attribute.'   ,
			    'restored'            => 'There was an error in restoring the :attribute.' ,
			    'updated'             => 'There was an error in updating the :attribute'  ,
	    	]
	    ],
	    'info'	=> [
	    	'title' => 'Information',
	    	'icon'	=> 'fa fa-info-circle',
	    	'class' => 'info',
	    	'messages' => [
	    		'created'             => 'There was an error in creating the :attribute.'  ,
			    'deleted'             => 'There was an error in deleting the :attribute.'  ,
			    'deleted_permanently' => ':attribute has been deleted permanently.'   ,
			    'restored'            => ':attribute has been successfully restored.' ,
			    'updated'             => ':attribute has been successfully updated.'  ,
	    	]
	    ],
	    'warning'	=> [
	    	'title' => 'Warning',
	    	'icon'	=> 'fa fa-exclamation',
	    	'class' => 'warning',
	    	'messages' => [
	    		'created'             => 'There was an error in creating the :attribute.'  ,
			    'deleted'             => 'There was an error in deleting the :attribute.'  ,
			    'deleted_permanently' => ':attribute has been deleted permanently.'   ,
			    'restored'            => ':attribute has been successfully restored.' ,
			    'updated'             => ':attribute has been successfully updated.'  ,
	    	]
	    ]
	],
];