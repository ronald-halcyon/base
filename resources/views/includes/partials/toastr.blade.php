<link rel="stylesheet" href="{{ asset('css/vendor/toastr/toastr.css') }}">
<script src="{{ asset('js/vendor/toastr/toastr.js') }}"></script>
<script type="text/javascript">

	var toast = toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}

	function success(message) {
		toastr.success(message);
	}

	function info(message) {
		toastr.info(message);
	}

	function error(message) {
		toastr.error(message);
	}

	function warning(message) {
		toastr.warning(message);
	}
</script>
