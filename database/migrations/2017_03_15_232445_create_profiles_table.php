<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string(config('profile.fields.first_name'));
            $table->char(config('profile.fields.middle_name'), 1)->nullable();
            $table->string(config('profile.fields.last_name'));
            $table->string(config('profile.fields.number'));
            $table->text(config('profile.fields.picture'))->nullable();
            $table->integer(config('profile.fields.relation_id'))->unsigned();
            $table->timestamps();
            $table->foreign(config('profile.fields.relation_id'))
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
