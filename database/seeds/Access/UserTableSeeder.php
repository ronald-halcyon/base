<?php

use Database\TruncateTable;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate(config('access.users_table'));

        //Add the master administrator, user id of 1
        $user_model                 = config('access.user');
        $user                       = new $user_model();
        $user->username             = "admin";
        $user->email                = "admin@admin.com";
        $user->password             = bcrypt('Admin!123');
        $user->confirmation_code    = md5(uniqid(mt_rand(), true));
        $user->confirmed            = true;
        $user->created_at           = Carbon::now();
        $user->updated_at           = Carbon::now();
        $user->save();
        if(config('profile.relation.change_profile')){
            $profile                  = [];
            $profile['relation_id'  ] = $user->id;
            $profile['first_name'   ] = "Admin";
            $profile['middle_name'  ] = "I";
            $profile['last_name'    ] = "Strator";
            $profile['number'       ] = "09267029952";
            $profile['picture'      ] = null;
            $profile                  = $user->profile()->create($profile);
        }

        $user_model                 = config('access.user');
        $user                       = new $user_model();
        $user->username             = "executive";
        $user->email                = "executive@executive.com";
        $user->password             = bcrypt('Executive!123');
        $user->confirmation_code    = md5(uniqid(mt_rand(), true));
        $user->confirmed            = true;
        $user->created_at           = Carbon::now();
        $user->updated_at           = Carbon::now();
        $user->save();
        if(config('profile.relation.change_profile')){
            $profile                  = [];
            $profile['relation_id'  ] = $user->id;
            $profile['first_name'   ] = "Client";
            $profile['middle_name'  ] = "E";
            $profile['last_name'    ] = "Executive";
            $profile['number'       ] = "09267029952";
            $profile['picture'      ] = null;
            $profile                  = $user->profile()->create($profile);
        }

        $user_model                 = config('access.user');
        $user                       = new $user_model();
        $user->username             = "user";
        $user->email                = "user@user.com";
        $user->password             = bcrypt('User!123');
        $user->confirmation_code    = md5(uniqid(mt_rand(), true));
        $user->confirmed            = true;
        $user->created_at           = Carbon::now();
        $user->updated_at           = Carbon::now();
        $user->save();
        if(config('profile.relation.change_profile')){
            $profile                  = [];
            $profile['relation_id'  ] = $user->id;
            $profile['first_name'   ] = "Default";
            $profile['middle_name'  ] = "U";
            $profile['last_name'    ] = "User";
            $profile['number'       ] = "09267029952";
            $profile['picture'      ] = null;
            $profile                  = $user->profile()->create($profile);
        }



        // $viewBackend->save();
        // $users = [
        //     [
        //         'username'              => 'Admin Istrator',
        //         'email'             => 'admin@admin.com',
        //         'password'          => bcrypt('1234'),
        //         'confirmation_code' => md5(uniqid(mt_rand(), true)),
        //         'confirmed'         => true,
        //         'created_at'        => Carbon::now(),
        //         'updated_at'        => Carbon::now(),
        //     ],
        //     [
        //         'name'              => 'Backend User',
        //         'email'             => 'executive@executive.com',
        //         'password'          => bcrypt('1234'),
        //         'confirmation_code' => md5(uniqid(mt_rand(), true)),
        //         'confirmed'         => true,
        //         'created_at'        => Carbon::now(),
        //         'updated_at'        => Carbon::now(),
        //     ],
        //     [
        //         'name'              => 'Default User',
        //         'email'             => 'user@user.com',
        //         'password'          => bcrypt('1234'),
        //         'confirmation_code' => md5(uniqid(mt_rand(), true)),
        //         'confirmed'         => true,
        //         'created_at'        => Carbon::now(),
        //         'updated_at'        => Carbon::now(),
        //     ],
        // ];

        // DB::table(config('access.users_table'))->insert($users);

        $this->enableForeignKeys();
    }
}
