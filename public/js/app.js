$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
/**
 * Works same as array_get function of Laravel
 * @param array
 * @param key
 * @param defaultValue
 * @returns {*}
 */
var array_get = function (array, key, defaultValue) {
    "use strict";

    if (typeof defaultValue === 'undefined') {
        defaultValue = null;
    }

    var result;

    try {
        result = array[key];
    } catch (err) {
        result = defaultValue;
    }

    if(result === null) {
        result = defaultValue;
    }

    return result;
};

/**
 * Get the array/object length
 * @param array
 * @returns {number}
 */
var array_length = function (array) {
    "use strict";

    return _.size(array);
};

/**
 * Get the first element.
 * Passing n will return the first n elements of the array.
 * @param array
 * @param n
 * @returns {*}
 */
var array_first = function (array, n) {
    "use strict";

    return _.first(array, n);
};

/**
 * Get the first element.
 * Passing n will return the last n elements of the array.
 * @param array
 * @param n
 * @returns {*}
 */
var array_last = function (array, n) {
    "use strict";

    return _.last(array, n);
};

/**
 * Works same as dd function of Laravel
 */
var dd = function () {
    "use strict";
    console.log.apply(console, arguments);
};

/**
 * Json encode
 * @param object
 */
var json_encode = function (object) {
    "use strict";
    if (typeof object === 'undefined') {
        object = null;
    }
    return JSON.stringify(object);
};

/**
 * Json decode
 * @param jsonString
 * @param defaultValue
 * @returns {*}
 */
var json_decode = function (jsonString, defaultValue) {
    "use strict";
    if (typeof jsonString === 'string') {
        var result;
        try {
            result = $.parseJSON(jsonString);
        } catch (err) {
            result = defaultValue;
        }
        return result;
    }
    return jsonString;
};