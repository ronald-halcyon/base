<?php

/**
 * Global helpers file with misc functions.
 */
if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('access')) {
    /**
     * Access (lol) the Access:: facade as a simple function.
     */
    function access()
    {
        return app('access');
    }
}

if (! function_exists('history')) {
    /**
     * Access the history facade anywhere.
     */
    function history()
    {
        return app('history');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('includeRouteFiles')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function includeRouteFiles($folder)
    {
        $directory = $folder;
        $handle = opendir($directory);
        $directory_list = [$directory];

        while (false !== ($filename = readdir($handle))) {
            if ($filename != '.' && $filename != '..' && is_dir($directory.$filename)) {
                array_push($directory_list, $directory.$filename.'/');
            }
        }

        foreach ($directory_list as $directory) {
            foreach (glob($directory.'*.php') as $filename) {
                require $filename;
            }
        }
    }
}

if (! function_exists('getRtlCss')) {

    /**
     * The path being passed is generated by Laravel Mix manifest file
     * The webpack plugin takes the css filenames and appends rtl before the .css extension
     * So we take the original and place that in and send back the path.
     *
     * @param $path
     *
     * @return string
     */
    function getRtlCss($path)
    {
        $path = explode('/', $path);
        $filename = end($path);
        array_pop($path);
        $filename = rtrim($filename, '.css');

        return implode('/', $path).'/'.$filename.'.rtl.css';
    }
}



if (!function_exists('response_with_messages')) {
    /**
     * @param string|array $messages
     * @param bool $error
     * @param int $responseCode
     * @param array $data
     * @return array
     */
    function response_with_messages($messages, $error = false, $responseCode = null, $data = null)
    {
        $type = $error ? 'danger' : 'success';
        return [
            'error' => $error,
            'type'  => $type,
            'response_code' => $responseCode ?: Constants::SUCCESS_NO_CONTENT_CODE,
            'messages' => (array)$messages,
            'data' => $data
        ];
    }
}

if (!function_exists('user')) {
    /**
     * @param string|array $messages
     * @param bool $error
     * @param int $responseCode
     * @param array $data
     * @return array
     */
    function user()
    {
        return auth()->user();
    }
}

if (!function_exists('now')) {
    /**
     * @param string|array $messages
     * @param bool $error
     * @param int $responseCode
     * @param array $data
     * @return array
     */
    function now()
    {
        return \Carbon\Carbon::now();
    }
}


if (! function_exists('status')) {
    /**
     * Access (lol) the Access:: facade as a simple function.
     */
    function status()
    {
        return [1 => trans('base.status.active'), 0 => trans('base.status.disable')];
    }
}


if (! function_exists('btn')) {
    /**
     * Access (lol) the Access:: facade as a simple function.
     * @var btn = add|edit|delete|view|search|create|update|save_continue
     * @param $btn $href $class $attrs
     */
    function btn($btn, $attrs=[], $class=null, $small=false)
    {
        $button = 'base.buttons.' . $btn;
        $attributes = '';
        if(count($attrs)){
            foreach ($attrs as $a => $attr) {
                $attributes .= $a . '="' . $attr . '"';
            }
        }
        return 
        '<a class="'. trans($button.'.class') .' ' . $class .'" '. $attributes .'>
            <i class="'. trans($button.'.icon') .'" '. ($small ? 'data-toggle="tooltip" data-placement="top" title="' .trans($button.'.label') . '"' : '') .'></i> ' . 
            ($small ? '' : trans($button.'.label')) . 
        '</a>';
    }
}


if (! function_exists('button')) {
    /**
     * Access (lol) the Access:: facade as a simple function.
     * @var button = add|edit|delete|view|search|create|update|save_continue
     * @param $button $href $class $attrs
     */
    function button($btn, $attrs=[], $class=null, $small=false)
    {
        $button = 'base.buttons.' . $btn;
        $attributes = '';
        if(count($attrs)){
            foreach ($attrs as $a => $attr) {
                $attributes .= $a . '="' . $attr . '"';
            }
        }
        return 
        '<button class="'. trans($button.'.class') .' ' . $class .'" '. $attributes .'>
            <i class="'. trans($button.'.icon') .'" '. ($small ? 'data-toggle="tooltip" data-placement="top" title="' .trans($button.'.label') . '"' : '') .'></i> ' . 
            ($small ? '' : trans($button.'.label')) . 
        '</button>';
    }
}

if(! function_exists('flash_message')) {

    function flash_message($type, $message, $custom=null)
    {
        session()->flash($custom ? $custom: $type, $message);
    }

}
