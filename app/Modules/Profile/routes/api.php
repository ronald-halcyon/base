<?php

Route::group(array('module' => 'Profile', 'middleware' => ['api'], 'namespace' => 'App\Modules\Profile\Controllers'), function() {

    Route::resource('Profile', 'ProfileController');
    
});	
