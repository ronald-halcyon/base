<?php

Route::group(array('module' => 'Profile', 'middleware' => ['web'], 'namespace' => 'App\Modules\Profile\Controllers', 'prefix' => 'account'), function() {
	
	Route::get('profile', 				['as' => 'profile', 						'uses' => 'ProfileController@view'		]);
	Route::patch('profile', 			['as' => 'profile.update', 					'uses' => 'ProfileController@update'	]);

	Route::patch('profile/password', 	['as' => 'profile.password', 				'uses' => 'ProfileController@password'	]);
	Route::patch('profile/avatar', 		['as' => 'profile.avatar', 					'uses' => 'ProfileController@avatar'	]);
    
});	
