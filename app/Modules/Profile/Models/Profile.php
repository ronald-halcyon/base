<?php 

namespace App\Modules\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model 
{
	use ProfileAttributes;

	protected $table = 'profiles';
	protected $fillable = [
		'relation_id', 'first_name', 'middle_name', 'last_name', 'number'
	];


	public function relation()
	{
		return $this->belongsTo(app(config('profile.relation.model')), 'relation_id');
	}

}
