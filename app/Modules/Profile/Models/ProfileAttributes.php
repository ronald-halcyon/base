<?php 

namespace App\Modules\Profile\Models;

use Illuminate\Database\Eloquent\Model;

trait ProfileAttributes 
{

	public function getNameAttribute()
	{
		return $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name;
	}

	public function getPhotoAttribute()
	{
		return $this->picture ?  asset('profiles/' . $this->picture) : config('profile.photo');
	}
}
