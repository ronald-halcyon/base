<?php

namespace App\Modules\Profile;

use App\Services\Access\Access;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Finder\Finder;

/**
 * Class AccessServiceProvider.
 */
class ProfileServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Package boot method.
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $envConfigPath = app_path() . '/Modules/Profile';
        $config = app('config');
        foreach (Finder::create()->files()->name('config.php')->in($envConfigPath) as $file)
        {
            // Run through all PHP files in the current environment's config directory.
            // With each file, check if there's a current config key with the name.
            // If there's not, initialize it as an empty array.
            // Then, use array_replace_recursive() to merge the environment config values 
            // into the base values.
            
            $key_name = basename('profile', '.php');
            $old_values = $config->get($key_name) ?: [];
            $new_values = require $file->getRealPath();
            // Replace any matching values in the old config with the new ones.
            $config->set($key_name, array_replace_recursive($old_values, $new_values));
        }
    }

}
