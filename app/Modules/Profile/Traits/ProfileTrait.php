<?php

namespace App\Modules\Profile\Traits;

use App\Modules\Profile\Models\Profile;

trait ProfileTrait
{
	public function profile()
	{
		return $this->hasOne(Profile::class, 'id');
	}

	public function canChangeProfile()
	{
		return config('profile.relation.change_profile');
	}

	public function getNameAttribute()
	{
		return $this->profile ? $this->profile->name : $this->username;
	}

}