<?php 

return [
	'fields' => [
		'first_name' 	=> 'First Name',
		'middle_name' 	=> 'Middle Initial',
		'last_name' 	=> 'Last Name',
		'number'		=> 'Contact Number',
		'picture'		=> 'Avatar'

	]
];
