<?php

namespace App\Modules\Profile;

use App\Repositories\BaseRepository;
use App\Modules\Profile\Models\Profile;

use DB;
use Image;
use File;

class ProfileRepository extends BaseRepository
{
	/**
     * Associated Repository Model.
     */
    const MODEL = Profile::class;


	protected $model;

	function __construct(Profile $model)
	{
		$this->model = $model;
	}


	public function update($user, $request)
	{
		$data['first_name'	] = $request['first_name'	];
		$data['last_name'	] = $request['last_name'	];
		$data['middle_name'	] = $request['middle_name'	];
		$data['number'		] = $request['number' 		];
		DB::beginTransaction();
		try {
			$item = $user->profile;
			if(!$item){
				$result = $user->profile()->create($data);
			}else{
				$result = $item->update($data);
			}
			$item = $user->profile;
            DB::commit();
			return true;
		} catch (\Exception $exception) {
	        DB::rollback();
	        throw new \Exception($exception);
        	$this->exceptions(trans('base.alerts.failed.messages.updated', ['attribute' => 'Your Profile']));
		}
	}

	public function avatar($request)
	{
		$user 		= access()->user();
		$profile 	= $user->profile;
		$path 		= 'profiles';
		if(!file_exists($path)){ $folder = File::makeDirectory($path); }
		if($request->hasFile('avatar') && $profile){
			DB::beginTransaction();
			$avatar 	= $request->file('avatar');
			$name	= 'avatar_' . $user->id . '_' . time() . '.' . $avatar->getClientOriginalExtension();
			$path 	= $path . '/' . $name;
			try {
				if($profile && $profile->avatar){ File::delete('profiles/' . $profile->avatar); }
				Image::make($avatar->getRealPath())->resize(config('profile.dimensions.'), 300)->save($path);
				$result = $user->profile()->update(['picture' => $name]);
	            DB::commit();
				return response_with_messages(trans('base.alerts.success.messages.updated', ['attribute' => 'Your avatar']), false, \Constants::SUCCESS_CODE, $profile);
			} catch (\Exception $exception) {
		        DB::rollback();
       			$this->exceptions(trans('base.alerts.failed.messages.updated', ['attribute' => 'Your avatar']));
			}
		}
        $this->exceptions(trans('base.alerts.failed.messages.updated', ['attribute' => 'Your avatar']));
	}
}