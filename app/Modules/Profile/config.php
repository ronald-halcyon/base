<?php

use App\Models\Access\User\User;

return [
	/**
	 * 
	 * 
	 * Module Name
	 * 
	 * 
	 */
	'module' => [
		'name' 		=> 'Profile',
		'active' 	=> true,
	],

	'relation' 	=> [
		'model' 			=> User::class,
		'change_profile' 	=> true,
	],

	'permission'	=> [
		'name'			=> 'profile',
		'display_name'	=> 'Profile',
		'sort'			=> 3,
	],
	

	'fields'	=> [
		'id'			=> 'id',
		'first_name'	=> 'first_name',
		'middle_name'	=> 'middle_name',
		'last_name'		=> 'last_name',
		'number'		=> 'number',
		'picture'		=> 'picture',
		'relation_id'	=> 'relation_id'
	],

	'photo'	=> asset('img/profile-default.jpg'),
	'dimensions' => [
		'width'  => 300,
		'height' => 300
	]
];

