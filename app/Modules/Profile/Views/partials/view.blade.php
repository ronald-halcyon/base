@if ($logged_in_user->canChangeProfile())
    <tr>
        <th>{{ trans('Profile::profile.fields.first_name') }}</th>
        <td>{{ $logged_in_user->profile->first_name }}</td>
    </tr>
    @if($logged_in_user->profile->middle_name)
        <tr>
            <th>{{ trans('Profile::profile.fields.middle_name') }}</th>
            <td>{{ $logged_in_user->profile->middle_name }}</td>
        </tr>
    @endif
    <tr>
        <th>{{ trans('Profile::profile.fields.last_name') }}</th>
        <td>{{ $logged_in_user->profile->last_name }}</td>
    </tr>
    <tr>
        <th>{{ trans('Profile::profile.fields.number') }}</th>
        <td>{{ $logged_in_user->profile->number }}</td>
    </tr>
@endif