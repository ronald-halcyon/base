@if ($logged_in_user->canChangeProfile())
	<div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
	    <label for="avatar" class="col-md-4 control-label">
	        {{ trans('Profile::profile.fields.picture') }}
	    </label>
	    <div class="col-md-6">
	    	<div id="dropzoneProfile" class="dropzone text-center">
			    <div class="dz-message">
				    <img id="profilePhoto" class="profile-user-img img-responsive" src="{{ $logged_in_user->picture }}" />
			    </div>
			</div>
	        @if ($errors->has('avatar'))
	            <span class="help-block">
	                <strong>{{ $errors->first('avatar') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>
	@section('after-styles')
		<link rel="stylesheet" type="text/css" href="{{ asset('css/vendor/dropzone/dropzone.min.css') }}">
		<style type="text/css">
			.dropzone{
			    border: 5px dotted #ECF0F5;
			    border-radius: 30px;
			    min-height: 100px;
			    padding: 0;
			}
			.dropzone #profilePhoto{
				margin: 0 auto;
				padding-bottom: 10px;
			}
		</style>
	@append

	@section('after-scripts')
		<script type="text/javascript" src="{{ asset('js/vendor/dropzone/dropzone.min.js') }}"></script>
		<script type="text/javascript">
			Dropzone.autoDiscover = false;
			var dropzone = new Dropzone('#dropzoneProfile', {
			    url: "{{ route('profile.avatar') }}",
			    // autoProcessQueue: false,
			    paramName: "avatar", // The name that will be used to transfer the file
			    maxFiles: 1,
			    maxFilesize: 2, // MB
			    uploadMultiple: false,
			    acceptedFiles: "image/*",
				addRemoveLinks: true,
			    init: function() {
			        var myDropzone = this;
			        this.on("addedfile", function(file) {
			            if(this.files[1] != null) {
			                this.removeFile(this.files[0]);
			            }
			            $('#profilePhoto').hide();
			        });
			        this.on("removedfile", function(file) {
				        $('#profilePhoto').show();
			        });

				    this.on("success", function(files, response) {
						success(response);
						location.reload();
				    });
				    this.on("error", function(files, response) {
						danger(response);
						location.reload();
				    });
				    this.on("sending", function(file, xhr, formData) {
						formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
						formData.append("_method", 'PATCH');
				    });
			    },
			});
		</script>
	@append
@endif