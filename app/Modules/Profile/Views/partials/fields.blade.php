@if ($logged_in_user->canChangeProfile())
    <div class="form-group">
        {{ Form::label('first_name', trans('Profile::profile.fields.first_name'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('first_name', 
            	(isset($profile) && $profile->first_name) ? $profile->first_name : old('first_name'), 
            	['class' => 'form-control', 'placeholder' => trans('Profile::profile.fields.first_name'), 'required']
            ) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('middle_name', trans('Profile::profile.fields.middle_name'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('middle_name', 
            	(isset($profile) && $profile->middle_name) ? $profile->middle_name : old('middle_name'), 
            	['class' => 'form-control', 'placeholder' => trans('Profile::profile.fields.middle_name'), 'min' => 1, 'max' => 1]
            ) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('last_name', trans('Profile::profile.fields.last_name'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('last_name', 
            	(isset($profile) && $profile->last_name) ? $profile->last_name : old('last_name'), 
            	['class' => 'form-control', 'placeholder' => trans('Profile::profile.fields.last_name'), 'required']
            ) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('number', trans('Profile::profile.fields.number'), ['class' => 'col-md-4 control-label', 'required']) }}
        <div class="col-md-6">
            {{ Form::number('number', 
            	(isset($profile) && $profile->number) ? $profile->number : old('number'), 
            	['class' => 'form-control', 'placeholder' => trans('Profile::profile.fields.number'), 'required']
            ) }}
        </div>
    </div>

@endif