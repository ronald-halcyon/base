<?php 

namespace App\Modules\Profile\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\Profile\ProfileRepository as Repository;

class ProfileController extends Controller 
{
	protected $repo;

	function __construct(Repository $repo)
	{
		$this->repo = $repo;
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function avatar(AvatarRequest $request)
	{
		$result = $this->repo->avatar($request);
		// if($request->ajax()){
		return json_encode($result['messages'][0]);
		// }
		// flash_message($result['type'], $result['messages']);
		// return redirect()->back();
	}

}
